config.load_autoconfig()

c.url.start_pages                   = "https://s.unover.se"
c.url.searchengines                 = {"DEFAULT": "https://s.unover.se/search?q={}", "srx": "https://searx.be/search?q={}", "ddg": "https://duckduckgo.com/?q={}", "aw": "https://wiki.archlinux.org/index.php?search={}", "vp": "https://voidlinux.org/packages/?arch=x86_64&q={}"}

c.content.javascript.enabled                =   False
c.content.blocking.enabled                  =   False
c.content.private_browsing                  =   True
c.completion.cmd_history_max_items          =   0
c.editor.command                            =   [ 'alacritty' '{}' ] # '--config-file', '/home/shv/alacritty/mf.yml', '--class', 'quvim', '-e', 'nvim', '{}' ]
c.colors.webpage.preferred_color_scheme     =   'dark'
c.tabs.show                                 =   'multiple'
c.downloads.position                        =   'bottom'

# ----------------
# bindings
config.bind (',m', 'hint links spawn mpv {hint-url}')
config.bind (',d', 'hint links spawn dlv {hint-url}')
config.bind (',e', 'edit-text')
config.bind (',t', 'config-cycle tabs.show never always')

# ----------------
# fonts
c.fonts.default_family = "Terminus"
c.fonts.default_size = "9pt"

# ----------------
# theme
config.source('nord.py')
