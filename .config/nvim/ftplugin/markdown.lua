vim.opt.wrap = true -- wrap text
vim.opt.breakindent = true -- match indent on line break
vim.opt.linebreak = true -- line break on whole words

-- allow j/k between wrapped lines
vim.keymap.set("n", "j", "gj")
vim.keymap.set("n", "k", "gk")

vim.opt.spell = true
