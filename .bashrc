source $HOME/.config/bash/alias
source $HOME/.config/bash/git-prompt.sh

PS1="\W\[\033[33m\]\$(GIT_PS1_SHOWUNTRACKEDFILES=1 GIT_PS1_SHOWDIRTYSTATE=1 __git_ps1)\[\033[00m\] \[\e[31m\]→\[\e[00m\] "

# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

shopt -s autocd				# change dir without cd
set -o vi

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

bind TAB:menu-complete
bind '"\e[Z": menu-complete-backward'
bind "set show-all-if-ambiguous on"
bind "set menu-complete-display-prefix on"
bind "set completion-ignore-case on"

# environment variables
export PATH="$PATH:$HOME/.config/scripts"
export PATH="$PATH:$HOME/.local/bin"
# export PATH="$PATH:$HOME/.local/share/AppImage"
export PATH="$PATH:$HOME/.nix-profile/bin"
export PF_INFO='ascii title os kernel wm pkgs memory uptime palette'
export TERM='alacritty'

#export BORG_PASSCOMMAND="pass show borg/desktop"

eval "$(zoxide init bash)"

# fix for waybar
export $(dbus-launch)
